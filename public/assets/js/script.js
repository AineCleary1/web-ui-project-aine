//using the jQuery library 
//making the answers(paragraphs) appear and dissapear when the
//the question (h2) is clicked
$(document).ready(function(){
    $(".drop").hide();
    $(".nondrop").click(function(){
        $(this).next(".drop").slideToggle(300);
    });
});

//js for the slideshow for sights page
let slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n){
    showDivs(slideIndex += n);
}

function showDivs(n){
    let y;
    let x = document.getElementsByClassName("slideImage");
    if(n > x.length){
        slideIndex = 1;
    }
    if(n < 1){
        slideIndex = x.length;
    }
    for(y = 0; y < x.length; y++){
        x[y].style.display = "none";
    }
    x[slideIndex-1].style.display = "block";
}

//for alert when button for a room button clicked
function roomBook(){
    alert("Thank You for Booking a Room");
}

//for alert when contact form submitted
function formFilled(){
    alert("Thank You for contacting us, we will get back to you as soon as possible.");
}

roomBook();
formFilled();